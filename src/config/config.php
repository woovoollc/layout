<?php

return [
    'width' => [],
    'height' => [],
    'min-width' => [],
    'min-height' => [],
    'max-width' => [],
    'max-height' => [],
    'position' => [
        'static',
        'relative',
        'absolute',
        'fixed'
    ]
];