<?php

namespace Layout;

use Illuminate\Support\ServiceProvider;

class LayoutServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton('layout', function () {
            return new Common();
        });
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadViewsFrom(__DIR__ . '/../views', 'layout');

        $this->publishes([
            __DIR__ . '/../config/config.php' => config_path('vendor/layout.php'),
            __DIR__ . '/../views' => resource_path('views/vendor/layout')
        ], 'style-layout');
    }
}