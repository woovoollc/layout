<div class="styles">
    <div class="styles-group">
        <div class="styles-header dropped" onclick="toggleLayout(this)">
            <span>Layout</span>
        </div>

        <div id="styles-layout">
            <div class="styles-option kit-field">
                <div class="kit-group">
                    <span>Width:</span>
                </div>
                <div class="kit-group">
                    <input type="text" id="width" class="kit-input"
                           onchange="window.setStyle('width', this.value)">
                </div>
                <div class="kit-group">
                    <span>Height:</span>
                </div>
                <div class="kit-group">
                    <input type="text" id="height" class="kit-input"
                           onchange="window.setStyle('height', this.value)">
                </div>

                <div class="kit-group">
                    <span>Min:</span>
                </div>
                <div class="kit-group">
                    <input type="text" id="min-width" class="kit-input"
                           onchange="window.setStyle('min-width', this.value)">
                </div>
                <div class="kit-group">
                    <span>Max:</span>
                </div>
                <div class="kit-group">
                    <input type="text" id="min-height" class="kit-input"
                           onchange="window.setStyle('max-height', this.value)">
                </div>

                <div class="kit-group">
                    <span>Max:</span>
                </div>
                <div class="kit-group">
                    <input type="text" id="max-width" class="kit-input"
                           onchange="window.setStyle('max-width', this.value)">
                </div>
                <div class="kit-group">
                    <span>Min:</span>
                </div>
                <div class="kit-group">
                    <input type="text" id="max-height" class="kit-input"
                           onchange="window.setStyle('min-height', this.value)">
                </div>
            </div>

            <div class="styles-option">
                <span>Position:</span>
                <ul class="styles-css" id="position">
                    <li data-value="static" onclick="setPosition(this, 'static')">
                        static
                    </li>
                    <li data-value="relative"
                        onclick="setPosition(this, 'relative')">
                        rela
                    </li>
                    <li data-value="absolute"
                        onclick="setPosition(this, 'absolute')">
                        abso
                    </li>
                    <li data-value="fixed" onclick="setPosition(this, 'fixed')">
                        fixed
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>

<script>
    function setPosition(el, value) {
        let elements = document.getElementById('position').children;
        for (let i = 0; i < elements.length; i++) {
            elements[i].classList.remove('styles-selected');
        }
        el.classList.add('styles-selected');
        window.setStyle('position', value);
    }

    function toggleLayout(el) {
        if (el.classList.contains('dropped')) {
            el.classList.remove('dropped');
            document.getElementById('styles-layout').style.display = 'none';
        } else {
            el.classList.add('dropped');
            document.getElementById('styles-layout').style.display = 'block';
        }
    }
</script>